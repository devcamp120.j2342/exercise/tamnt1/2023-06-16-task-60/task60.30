package com.devcamp.countries.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.countries.model.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Integer> {
	@Query("SELECT c FROM CCountry c WHERE c.countryCode = :countryCode")
	CCountry findByCountryCode(@Param("countryCode") String countryCode);
}
