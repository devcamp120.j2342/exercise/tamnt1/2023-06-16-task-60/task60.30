package com.devcamp.countries.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countries.model.CCountry;
import com.devcamp.countries.model.CRegion;
import com.devcamp.countries.repository.CountryRepository;

@Service
public class CountryService {
    private final CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public void createCountries() {
        CCountry country1 = new CCountry("US", "United States");
        CCountry country2 = new CCountry("CA", "Canada");
        CCountry country3 = new CCountry("AU", "Australia");

        CRegion region1 = new CRegion("NY", "New York");

        country1.getRegions().add(region1);

        CRegion region2 = new CRegion("ON", "Ontario");

        country1.getRegions().add(region2);

        CRegion region3 = new CRegion("NSW", "New South Wales");
        country3.getRegions().add(region3);

        countryRepository.save(country1);
        countryRepository.save(country2);
        countryRepository.save(country3);
    }

    public List<CCountry> getCountries() {
        return countryRepository.findAll();
    }
}
