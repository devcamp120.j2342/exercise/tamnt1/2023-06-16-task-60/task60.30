package com.devcamp.countries.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countries.model.CCountry;
import com.devcamp.countries.model.CRegion;
import com.devcamp.countries.repository.CountryRepository;
import com.devcamp.countries.repository.RegionRepository;

@Service
public class RegionService {
    private final RegionRepository regionRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public RegionService(RegionRepository regionRepository, CountryRepository countryRepository) {
        this.regionRepository = regionRepository;
        this.countryRepository = countryRepository;
    }

    public void createRegions() {

    }

    public List<CRegion> getRegions() {
        return regionRepository.findAll();
    }

    public Set<CRegion> getRegionByCountryCode(String countryCode) {
        CCountry country = countryRepository.findByCountryCode(countryCode);
        System.out.println(country);
        if (country != null) {
            return country.getRegions();
        } else {

            return Collections.emptySet();
        }
    }
}
