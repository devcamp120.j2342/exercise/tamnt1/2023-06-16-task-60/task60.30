package com.devcamp.countries.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countries.model.CCountry;
import com.devcamp.countries.model.CRegion;
import com.devcamp.countries.services.RegionService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class RegionController {

    @Autowired
    RegionService regionService;

    @GetMapping("/create-region")
    public void createRegions() {
        regionService.createRegions();
    }

    @GetMapping("/regions")
    public List<CRegion> regions() {
        return regionService.getRegions();
    }

    @GetMapping("/region")
    public Set<CRegion> getRegionsByCountryCode(@RequestParam("countryCode") String countryCode) {
        return regionService.getRegionByCountryCode(countryCode);
    }
}
