package com.devcamp.countries.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countries.model.CCountry;

import com.devcamp.countries.services.CountryService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping("/create-countries")
    public void createCountries() {
        countryService.createCountries();
    }

    @GetMapping("/countries")
    public List<CCountry> countries() {
        return countryService.getCountries();
    }

}
